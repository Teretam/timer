$('[data-toggle="tooltip"]').tooltip();

var setLocation = "http://" + document.location.hostname + "";

$("#start").click(function() {
    var send = $.post(setLocation + '/ajax/settime', { data: "" })  
    .done(function(data) {
        console.log(data);
        location.reload();
        //$(".title").val(data.title);
    }).fail(function(data) {
        console.log(data);
    }).always(function(data) {
        console.log(data);
    });
});


$("#stop").click(function() {
    //var url = $(".url").val();
    var send = $.post(setLocation + '/ajax/stoptime', { data: "" })  
    .done(function(data) {
        console.log(data);
        location.reload();
        //$(".title").val(data.title);
    }).fail(function(data) {
        console.log(data);
    }).always(function(data) {
        console.log(data);
    });
});


// orginal 28-01-2015, 15:41:23 
// po 2015,01,28,15,41,23

//stara 2014,8,26,2,1,0,0



$('.finishTime').each(function(){

    var getStartTime = $(this).attr("data-test");
    var target_date = new Date(getStartTime).getTime();
    var days, hours, minutes, seconds;

    //$(this).html(countdown);

    setInterval(function () {
        var current_date = new Date().getTime();
        var seconds_left = (target_date - current_date) / 1000;
        days = parseInt(seconds_left / 86400);
        seconds_left = seconds_left % 86400;
        hours = parseInt(seconds_left / 3600);
        seconds_left = seconds_left % 3600;
        minutes = parseInt(seconds_left / 60);
        seconds = parseInt(seconds_left % 60);

        $('.days').html(days);
        $('.hours').html(hours);
        $('.minutes').html(minutes);
        $('.seconds').html(seconds);

        var getData = $('.days').html();
        if(getData == '0' ) {
            $('.days').html('0');
        } else {
          $('.days').html($('.days').html().substr(1));  
        }
        $('.hours').html($('.hours').html().substr(1));
        $('.minutes').html($('.minutes').html().substr(1));
        $('.seconds').html($('.seconds').html().substr(1));

    }, 1000);

 });




