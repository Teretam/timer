module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        jshint: {
            files: ['web/js/*.js'],
        },
        watch: {
            options: {
                livereload: true,
            },
            symfony: {
              files: [ 'src/tereBundle/Controller/**/*',
                       'src/tereBundle/Form/**/*',
                       'src/tereBundle/Entity/**/*',
                       'src/tereBundle/Menu/**/*',
                       'src/tereBundle/Resources/views/**/*.html.twig', '!src/tereBundle/Resources/views/layout.html.twig',
                       'app/config/**/*', 
                       'app/AppKernel.php'
              ],
              tasks: ['build']
            },

            html: {
                files: ['src/tereBundle/Resources/views/base.html.twig'],
                tasks: ['build']  
            },
            js: {
                files: ['Gruntfile.js', 'web/js/main.js'],
                tasks: ['build']
            },
            css: {
                files: ['web/css/main.css'],
                tasks: ['build']
            }
        },

        wiredep: {
            target: {
                src: 'src/tereBundle/Resources/views/base.html.twig',
                overrides: {
                    fancybox: {
                        'main': [
                            'source/jquery.fancybox.js',
                            'source/jquery.fancybox.css'
                        ]
                  }
                }
              }
            },

        copy: {
            main: {
                files: [
                    {   src: ['src/tereBundle/Resources/views/base.html.twig'], 
                        dest: 'src/tereBundle/Resources/views/layout.html.twig' },
                    {   src: ['web/css/main.css'], 
                        dest: '.tmp/concat/css/main.css' },
                    {   src: ['web/js/main.js'], 
                        dest: '.tmp/concat/js/main.js' },
                ]
            },
            bootstrap: {
                files: [
                    {
                        expand: true,
                        filter: 'isFile',
                        cwd: 'bower_components/bootstrap/dist',
                        dest: 'web',
                        src: ['fonts/*']
                    }
                ]
            },
            fontawesome: {
                files: [
                    {
                        expand: true,
                        filter: 'isFile',
                        cwd: 'bower_components/fontawesome',
                        dest: 'web',
                        src: ['fonts/*']
                    }
                ]
            },
            fancybox: {
                files: [
                    {
                        expand: true,
                        filter: 'isFile',
                        cwd: 'bower_components/fancybox/source',
                        dest: 'web/media/',
                        src: ['*.gif', '*.png']
                    }
                ]
            },
            css: {
                src: ['.tmp/css/**'],
                dest: 'web/css/optimized.css',
            },
            js: {
                src: ['.tmp/js/**'],
                dest: 'web/js/optimized.js',
            }                                   
        },

        useminPrepare: {
            html: 'src/tereBundle/Resources/views/base.html.twig',
            options: {
                root: 'src/tereBundle/Resources/views',
                dest: '.tmp'
            }
        },

        usemin: {
            html: 'src/tereBundle/Resources/views/layout.html.twig',
            options: {
                blockReplacements: {
                    less: function(block) {
                        return '<link rel="stylesheet" href="' + block.dest + '">';
                    }
                },
                dirs: ['src/tereBundle/Resources/views']
            }
        },

        htmlmin: {
            dist: { 
                options: { 
                    removeComments: true,
                    collapseWhitespace: true,
                    removeEmptyAttributes: true,
                    keepClosingSlash: true,
                    removeCommentsFromCDATA: true,
                    useShortDoctype: false,
                    removeScriptTypeAttributes: false,
                    removeStyleLinkTypeAttributes: false 
                    /*
                    collapseBooleanAttributes: true,
                    collapseWhitespace: true,
                    removeAttributeQuotes: false,
                    removeComments: true,
                    removeEmptyAttributes: true,
                    removeRedundantAttributes: true,

                    */
                },
                files: { // 'destination': 'source'
                    'src/tereBundle/Resources/views/layout.html.twig': 'src/tereBundle/Resources/views/layout.html.twig' 
                }
            }
        }

    });

    // Actually running things.
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-htmlmin');
    grunt.loadNpmTasks('grunt-wiredep');

    grunt.loadNpmTasks('grunt-usemin');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-font-awesome-vars');

    // Default task(s).
    grunt.registerTask('build', [
        'wiredep',
        'copy:main',
        'copy:bootstrap',
        'copy:fontawesome',
        'copy:fancybox',
        'useminPrepare',
        'usemin',
        'concat:generated',
        'cssmin:generated',
        'uglify:generated',
        'copy:css',
        'copy:js',
        'htmlmin'
    ]);

    grunt.registerTask('default', ['watch']);

};