<?php

namespace tereBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Session\Session;

class AdminController extends Controller {

    /**
     * @Route("/admin", name="admin")
     * @Template("tereBundle:Admin:index.html.twig")
     */
    public function indexAction(){
    	return array();
    }
}