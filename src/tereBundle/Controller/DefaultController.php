<?php

namespace tereBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use tereBundle\Entity\Timer;

class DefaultController extends Controller {

    /**
     * @Route("/", name="index")
     * @Template("tereBundle:Default:index.html.twig")
     */
    public function indexAction(){

        if( $this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY') ){
    		$em = $this->getDoctrine()->getManager();
    		$user = $this->get('security.context')->getToken()->getUser()->getId();
            
    		$timer = $em->getRepository('tereBundle:Timer')->findBy(array('user' => $user),array());



            $getTimes = array();
            foreach ($timer as $t => $time) {
                $timerStart = $time->getStart()->format("d-m-Y, G:i:s");
                
                if($time->getStop()) {
                    $timerStop = $time->getStop()->format("d-m-Y, G:i:s");
                    $dateStop = new \DateTime($timerStop);
                    $dateStart = new \DateTime($timerStart);
                    $getTime = $dateStop->diff($dateStart);
                    $getTimes[$t]['time']= $getTime;
                } 
            }



            return array(
                'timer'     => $timer,
                'getTimes'  => $getTimes
            );
        } else {
            return array();
        }
    }    
}