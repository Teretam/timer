<?php

namespace tereBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

use tereBundle\Entity\Timer;

class AjaxController extends Controller {


	/**
     * @Route("/ajax/settime", name="ajax_setTime")
     */
    public function setTimeAction(request $request) {

	    $isAjax = $this->get('Request')->isXMLHttpRequest();
	    if ($isAjax) {         
			if( $this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY') ){
				$em = $this->getDoctrine()->getManager();
			    $user = $this->get('security.context')->getToken()->getUser()->getId();
		    	$request = $this->get('request');
				$data = $request->request->get('data'); 
				$timer= new Timer();
				$timer->setStart(new \DateTime());
				$timer->setCreated(new \DateTime());
				$timer->setUser($user);
				$em->persist($timer);
				$em->flush();   
	            $return = array("test");
			}
			$return = json_encode($return); 
			return new Response($return, 200, array('Content-Type'=>'application/json; charset=utf-8'));
	    }
	    return new Response('', 400);
    }

	/**
     * @Route("/ajax/stoptime", name="ajax_stopTime")
     */
    public function stopTimeAction(request $request) {

	    $isAjax = $this->get('Request')->isXMLHttpRequest();
	    if ($isAjax) {         
			if( $this->container->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY') ){
				$em = $this->getDoctrine()->getManager();
			    $user = $this->get('security.context')->getToken()->getUser()->getId();
		    	$request = $this->get('request');
				$data = $request->request->get('data'); 
				$lastTime = $em->getRepository('tereBundle:Timer')->findOneBy(array('user' => $user, 'stop' => null ),array());
				$lastTime->setStop(new \DateTime());
				$em->persist($lastTime);
				$em->flush();   
	            $return = array("test");
			}
			$return = json_encode($return); 
			return new Response($return, 200, array('Content-Type'=>'application/json; charset=utf-8'));
	    }

	    return new Response('', 400);

    }

}