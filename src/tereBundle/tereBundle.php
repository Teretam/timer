<?php

namespace tereBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class tereBundle extends Bundle {

    public function getParent() {
        return 'FOSUserBundle';
    }

}